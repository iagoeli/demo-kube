const express       = require("express");
const MongoClient   = require("mongodb").MongoClient
const assert        = require("assert");
const bodyParser    = require("body-parser");

const app = express();
app.use(bodyParser());

const url = process.env.MONGO_URL
var collection;

MongoClient.connect(url, (err, client) => {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    const db = client.db('beleza');
    collection = db.collection('maumau');
    app.listen(3002, () => {
        console.log('listening on 3002');
    });
});

app.post('/', (req, res) => {
    console.log(req.body);
    let result = collection.insertOne({newValue: req.body.beleza});
    res.send(result).status(200);
});

app.get('/', (req, res) => {
    collection.find({}).toArray((err, docs) => {
        res.send(docs);
    })
});